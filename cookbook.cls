\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{cookbook}[2022/01/23 Recipe latex class]

\LoadClass[twoside, 11pt]{book}

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFGURATIONS
%----------------------------------------------------------------------------------------

\RequirePackage{fancyhdr}
\RequirePackage{multicol}
\RequirePackage{geometry}

\RequirePackage{enumitem}
\RequirePackage{textcomp}
\RequirePackage{gensymb}
\RequirePackage{subfig}
\RequirePackage{float}
\RequirePackage{titlesec}
\RequirePackage{titletoc}
\RequirePackage[toc]{multitoc}
\RequirePackage{wrapfig}
\RequirePackage{afterpage}

\RequirePackage[framemethod=tikz]{mdframed} % Allows defining custom boxed/framed environments
\RequirePackage{graphicx}
\RequirePackage[export]{adjustbox}

\RequirePackage{fontspec}

\graphicspath{ {./Pictures/} }


%----------------------------------------------------------------------------------------
%	FONTS
%----------------------------------------------------------------------------------------

\newfontfamily\headingfont[]{Chancery Uralic}

\newfontfamily{\chapterfont}[Scale=3]{Chancery Uralic}


%----------------------------------------------------------------------------------------
%	DOCUMENT MARGINS, HEADER, FOOTER, FORMATTING
%----------------------------------------------------------------------------------------
\geometry{
	paper=letterpaper, % Paper size, change to letterpaper for US letter size
	%top=1in, % Top margin
	%bottom=1in, % Bottom margin
	%left=1.5in, % Left margin
	%right=1in, % Right margin
	headheight=14pt, % Header height
	footskip=1.5cm, % Space from the bottom margin to the baseline of the footer
	headsep=1.0cm, % Space from the top margin to the baseline of the header
	%showframe, % Uncomment to show how the type block is set on the page
	inner=1.25in,
	outer=1in, 
	vmargin=1in
}

\titleformat*
{\section} % command
{\Huge\headingfont} % format

\titleformat
{\chapter} % command
[display] % shape
{\centering\Huge\chapterfont} % format
{\thechapter} % label
{} % sep
{\thispagestyle{empty} } % before-code
[] % after-code


%----------------------------------------------------------------------------------------
%	TABLE OF CONTENTS
%----------------------------------------------------------------------------------------
\renewcommand*{\multicolumntoc}{2}

\renewcommand{\contentsname}{\large Table of Contents}


%----------------------------------------------------------------------------------------
%	CUSTOM COMMANDS
%----------------------------------------------------------------------------------------

\newcommand{\recipe}[1]
{%%
    \section*{#1}
    \addcontentsline{toc}{section}{#1}
}

\newcommand{\dedication}
{%%
    \section*
}

% fraction bold
\newcommand{\fracb}[3]
{%%
    $\mathbf{#1\frac{#2}{#3}}$
}

% fraction normal
\newcommand{\fracn}[3]
{%%
    $#1\frac{#2}{#3}$
}

\newcommand{\mypage}
{%%
  \raisebox{2 pt}[0pt][0pt]{%%
  \rule[-1ex]{1cm}{3.5ex}%%
  \hspace*{-0.5cm}%%
  \makebox[0pt]{\textcolor{white}{\textbf\thepage}}%%
  \hspace*{0.6cm}}%%
}

\newcommand{\kitchenof}[1]
{%%
    \hspace{0em}\normalsize\textbf{From The Kitchen of {\Large\headingfont#1 \\}} 
    \hrule height 0.05cm
    \bigskip\par
}

\newcommand{\ingredients}[1][Ingredients]
{%%
    {
    \setlength\parskip{0.7cm}
    \noindent
    \textbf{\large#1}
    \vspace{2 mm}
    }
    \raggedright
}

\newcommand{\instructions}[1][Instructions]
{%%
    {
    \setlength\parskip{0.7cm}
    \noindent
    \textbf{\large #1}
    \vspace{2 mm}
    }
}
    
\newcommand{\temp}[1]
{%%
    $#1^\circ$C
}

\newcommand\blankpage
{%%
    \null
    \thispagestyle{empty}%
    %\addtocounter{page}{-1}%
    \newpage
}


%----------------------------------------------------------------------------------------
%	SECTION AND CHAPTER FORMATTING
%----------------------------------------------------------------------------------------

\setlength{\itemsep}{4pt}
\setlength{\parskip}{0pt}
\setlength{\parsep}{0pt}

\pagestyle{fancy}
\fancyfoot{}
\fancyhead[R]{}
\fancyhead[L]{}
\fancyhead[C]{}
\renewcommand{\headrulewidth}{0.0pt}
\renewcommand{\footrulewidth}{0.0pt}
\fancyfoot[LE,RO]{\mypage} % Custom \mypage to replace \thepage in custom commands section



%----------------------------------------------------------------------------------------
%	CUSTOM LISTS
%----------------------------------------------------------------------------------------

\newlist{ingredientslist}{enumerate}{2}
\setlist[ingredientslist]
{%%
    font=\sffamily\bfseries,
    itemsep=2pt,
    parsep=0.00cm,
    topsep=0pt,
}

\newlist{instructionslist}{enumerate}{2}
\setlist[instructionslist]
{%%
    font=\sffamily\bfseries,
    itemsep=10pt,
    parsep=0.00cm,
    topsep=0pt
}
