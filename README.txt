This folder is all the contents of the Barrick Family Cookbook

The pdfs are broken into three parts, black and white, color, and cover and back

The color pdfs are broken into 8 parts so printing is possible. In order to 
print the color portions canot be too large. The front cover and back 
cover are in a separate pdf and is the only document that should be printed 
single sided. All other pdfs can be printed double sided. 

There is one pdf that is just called Cookbook.pdf. It is the entire book 
assembled in one document. This can be used for online viewing, or for
reference when attempting to put all the pages in order after printing. 

This folder also includes all the source code for the cookbook. It was 
rendered into a pdf using a tool called Latex. If you want to look at the 
code and see how it was done it is fair game. The source code for the book
is what will allow it to be generated again and again in the event a pdf is 
not available.  

For more information visit https://www.latex-project.org

Love you all,
Jake
